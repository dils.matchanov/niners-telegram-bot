def search_element(element, array):
    found = False

    for i in array:
        if element in i:
            found = True
            break
    
    return found