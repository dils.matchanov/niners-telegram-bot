import logging
import telegram
from telegram.ext import Updater

import helpers
from models import NavigationItem


with open("key.txt") as k:
    key = k.readlines()[0].strip()

updater = Updater(token=key)
dispatcher = updater.dispatcher

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)


main_menu = [["Учебный центр"], ["Комментарии"], ["Задать вопрос?"]]
center_menu = [["Курсы"], ["Преподаватели"]]


message = "Welcome to niners educational center!"
main_menu_navigation = NavigationItem(title=None, message=message, next_navigations=main_menu)




def start(bot, update):
    reply_markup = telegram.ReplyKeyboardMarkup(main_menu_navigation.next_navigations)

    chat_id = update.message.chat_id
    bot.send_message(chat_id,
                     text=main_menu_navigation.message,
                     reply_markup=reply_markup)


def message(bot, update):
    chat_id = update.message.chat_id

    reply_markup = telegram.ReplyKeyboardRemove()

    if helpers.search_element(update.message.text, main_menu):
        center_reply_markup = telegram.ReplyKeyboardMarkup(center_menu)

        # TODO: think how to make it look better

        if update.message.text == "Учебный центр":
            message = "Choose a category"
            bot.send_message(chat_id, text=message, reply_markup=center_reply_markup)

        if update.message.text == "Комментарии":
            # something here
            pass
        
        if update.message.text == "Задать вопрос?":
            # something here
            pass



from telegram.ext import CommandHandler, MessageHandler, Filters


start_handler = CommandHandler("start", start)
message_handler = MessageHandler(Filters.text, message)

dispatcher.add_handler(start_handler)
dispatcher.add_handler(message_handler)


print("Bot started!")

updater.start_polling()

updater.idle()
